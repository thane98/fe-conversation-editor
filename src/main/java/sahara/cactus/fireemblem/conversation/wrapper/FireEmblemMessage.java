/*
 * Copyright (C) 2017 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.wrapper;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sahara.cactus.fireemblem.conversation.util.text.ScriptHelper;

/**
 * A simple wrapper class used to contain information about a message in a text
 * file.
 * 
 * @author Secretive Cactus
 */
public class FireEmblemMessage {
  
  /**
   * The name of the message. Used as its key when files are saved.
   */
  private String name;
  
  /**
   * The content of the message.
   */
  private final StringProperty content;
  
  /**
   * Creates a new message.
   * 
   * @param name the name of the message
   */
  public FireEmblemMessage(String name) {
    this.name = name;
    content = new SimpleStringProperty("");
  }
  
  /**
   * Creates a new message.
   * 
   * @param name the name of the message
   * @param content the content of the message
   */
  public FireEmblemMessage(String name, StringProperty content) {
    this.name = name;
    this.content = content;
  }
  
  /**
   * Returns the message's content property.
   * 
   * @return the message's content property.
   */
  public StringProperty contentProperty() {
    return content;
  }
  
  /**
   * Returns the message's content.
   * 
   * @return the message's content.
   */
  public String getContent() {
    return content.get();
  }
  
  /**
   * Returns the message's content formatted for saving to a file.
   * 
   * @return the message's content formatted for saving to a file.
   */
  public String getContentForOut() {
    return ScriptHelper.convertFromScript(content.get());
  }
  
  /**
   * Returns the message's content formatted for saving to a script file.
   * 
   * @return the message's content formatted for saving to a script file.
   */
  public String getContentForOutScript() {
    return content.get().replaceAll("\n", "\\\\/");
  }
  
  /**
   * Returns the message's name.
   * 
   * @return the message's name.
   */
  public String getName() {
    return name;
  }
  
  /**
   * Sets the message's name.
   * 
   * @param name the message's new name.
   */
  public void setName(String name) {
    this.name = name;
  }
}
