/*
 * Copyright (C) 2017 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.manager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import sahara.cactus.fireemblem.conversation.manager.Config.GameMode;
import sahara.cactus.fireemblem.conversation.manager.Config.GameMode.GameBranchMissingException;
import sahara.cactus.fireemblem.conversation.myunit.Kamui;
import sahara.cactus.fireemblem.conversation.myunit.MyUnit;
import sahara.cactus.fireemblem.conversation.myunit.Robin;
import sahara.cactus.fireemblem.conversation.util.text.TextToScriptConverter;
import sahara.cactus.fireemblem.conversation.util.text.ScriptToTextConverter;
import sahara.cactus.fireemblem.conversation.util.text.ScriptHelper;

/**
 * Handles the processing of XML data for a variety of purposes.
 * 
 * @author Secretive Cactus
 */
public class XMLManager {
  
  /**
   * Provides logging for the class.
   */
  private static final Logger LOGGER =
                                   Logger.getLogger(XMLManager.class.getName());
  
  /**
   * Contains all of the editor's translatable text.
   */
  private static final HashMap<String, StringProperty> TEXT = new HashMap<>();
  
  /**
   * Contains the list of core text files that need to be loaded when the game
   * mode changes.
   */
  private static final String[] CORE_TEXT_FILES = {
                                           "System", "GUI", "Script", "Override"
                                                  };
  
  /**
   * Gets a ButtonType designed for the passed type with text for the current
   * editor language.
   * 
   * @param type the type of button that should be created
   * @return a ButtonType with the appropriate text for the current language
   */
  public static ButtonType getButtonType(ButtonData type) {
    switch (type) {
      case OK_DONE:
        return new ButtonType(getText("OK"), type);
      case CANCEL_CLOSE:
        return new ButtonType(getText("Cancel"), type);
      case YES:
        return new ButtonType(getText("Yes"), type);
      case NO:
        return new ButtonType(getText("No"), type);
    }
    return null;
  }
  
  /**
   * Returns a NodeList of the children of the root node in the passed file.
   * 
   * @param file the file from which the nodes should be read
   * @return a NodeList of the nodes in the file
   */
  public static NodeList getNodesFromFile(File file) {
    try {
      DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
        .newInstance();
      DocumentBuilder documentBuilder =
                                    documentBuilderFactory.newDocumentBuilder();
      Document document = documentBuilder.parse(file);
      return document.getFirstChild().getChildNodes();
    } catch (ParserConfigurationException | SAXException | IOException ex) {
      LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
    }
    return null;
  }
  
  /**
   * Returns a NodeList of the nodes of a given type in the passed file.
   * 
   * @param file the file from which text should be read
   * @param name the name of the node type that should be read
   * @return a NodeList of the nodes of the given type
   */
  public static NodeList getNodesOfType(File file, String name) {
    try {
      DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
        .newInstance();
      DocumentBuilder documentBuilder =
                                    documentBuilderFactory.newDocumentBuilder();
      Document document = documentBuilder.parse(file);
      return document.getElementsByTagName(name);
    } catch (ParserConfigurationException | SAXException | IOException ex) {
      LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
    }
    return null;
  }
  
  /**
   * Returns the text value of the given key for the current language.
   * 
   * @param key the key for which text should be obtained
   * @return the text related to the key
   */
  public static String getText(String key) {
    return TEXT.get(key).get();
  }
  
  /**
   * Returns the text property of the given key for the current language.
   * 
   * @param key the key for which text should be obtained
   * @return the property related to the key
   */
  public static StringProperty getTextProperty(String key) {
    return TEXT.get(key);
  }
  
  /**
   * Returns an ArrayList of the text content of every node of the given type
   * in the passed file.
   * 
   * @param file the file from which text should be read
   * @param name the name of the node type from which content should be read
   * @return the content of every node of the given type in the file
   */
  public static ArrayList<String> getTextNodesOfType(File file, String name) {
    ArrayList<String> result = new ArrayList<>();
    try {
      DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
        .newInstance();
      DocumentBuilder documentBuilder =
                                    documentBuilderFactory.newDocumentBuilder();
      Document document = documentBuilder.parse(file);
      NodeList text = document.getElementsByTagName(name);
      int len = text.getLength();
      for (int i = 0; i < len; i++) {
        result.add(text.item(i).getTextContent());
      }
    } catch (ParserConfigurationException | SAXException | IOException ex) {
      LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
    }
    return result;
  }
  
  /**
   * Loads all of the editor's core text for the given language.
   * 
   * @param lang the language for which text should be loaded
   * @throws GameBranchMissingException when a branch has not been supplied for
   * a game mode
   */
  public static void loadLanguage(String lang) throws
                                                    GameBranchMissingException {
    File xml;
    for (String file : CORE_TEXT_FILES) {
      xml = ResourceManager.getFileResource("txt/xml/lang/" + lang + "/"  +
                                            file + ".xml");
      if (xml.exists()) {
        loadXMLAsProperties(xml, TEXT);
      }
    }
    if (Config.getGameMode() != null) {
      CharacterManager.loadCharacters();
    }
    ScriptHelper.reloadEmotions();
    TextToScriptConverter.reloadPatterns();
    ScriptToTextConverter.reloadPatterns();
  }
  
  /**
   * Loads the user-created My Units for the current game mode into the passed
   * list.
   * 
   * @param list a list into which the My Units should be loaded
   * @throws GameBranchMissingException when a branch has not been supplied for
   * a game mode
   */
  public static void loadMyUnits(ObservableList<MyUnit> list) throws
                                                    GameBranchMissingException {
    File xmlFile = ResourceManager.getFileResource("txt/xml/userdata/" +
                                                   "MyUnits.xml");
    try {
      DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
        .newInstance();
      DocumentBuilder documentBuilder =
                                    documentBuilderFactory.newDocumentBuilder();
      Document document = documentBuilder.parse(xmlFile);
      NodeList myUnits = document.getElementsByTagName("myunit");
      int len = myUnits.getLength();
      GameMode.branch(() -> {
        for (int i = 0; i < len; i++) {
          list.add(Robin.fromXML(myUnits.item(i).getChildNodes()));
        }
      }, () -> {
        for (int i = 0; i < len; i++) {
          list.add(Kamui.fromXML(myUnits.item(i).getChildNodes()));
        }
      });
    } catch (ParserConfigurationException | SAXException | IOException ex) {
      LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
    }
  }
  
  /**
   * Loads the XML translation strings in the passed file into the passed map
   * as instances of StringProperty.
   * 
   * @param file the file from which translation strings should be loaded
   * @param target the map into which the strings should be loaded
   */
  public static void loadXMLAsProperties(File file,
                                        HashMap<String,StringProperty> target) {
    try {
      
      DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
        .newInstance();
      DocumentBuilder documentBuilder =
                                    documentBuilderFactory.newDocumentBuilder();
      Document document = documentBuilder.parse(file);
      NodeList text = document.getElementsByTagName("translation");
      int len = text.getLength();
      StringProperty value;
      String name;
      Node node;
      for (int i = 0; i < len; i++) {
        node = text.item(i);
        name = node.getAttributes().getNamedItem("name").getNodeValue();
        if ((value = target.get(name)) != null) {
          value.set(node.getTextContent());
        } else {
          target.put(name, new SimpleStringProperty(node.getTextContent()));
        }
      }
      text = document.getElementsByTagName("emotion");
      len = text.getLength();
      for (int i = 0; i < len; i++) {
        node = text.item(i);
        name = node.getAttributes().getNamedItem("name").getNodeValue();
        if ((value = target.get(name)) != null) {
          value.set(node.getTextContent());
        } else {
          target.put(name, new SimpleStringProperty(node.getTextContent()));
        }
      }
    } catch (ParserConfigurationException | SAXException | IOException ex) {
      LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
    }
  }
  
  /**
   * Loads the XML translation strings in the passed file into the passed map.
   * 
   * @param file the file from which translation strings should be loaded
   * @param target the map into which the strings should be loaded
   */
  public static void loadXMLAsStrings(File file,
                                      HashMap<String,String> target) {
    try {
      DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
        .newInstance();
      DocumentBuilder documentBuilder =
                                    documentBuilderFactory.newDocumentBuilder();
      Document document = documentBuilder.parse(file);
      NodeList text = document.getElementsByTagName("translation");
      int len = text.getLength();
      String name;
      Node node;
      for (int i = 0; i < len; i++) {
        node = text.item(i);
        name = node.getAttributes().getNamedItem("name").getNodeValue();
        target.put(name, node.getTextContent());
      }
    } catch (ParserConfigurationException | SAXException | IOException ex) {
      LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
    }
  }
  
  /**
   * Loads the XML translation strings in the passed file into the passed map.
   * 
   * @param file the name of the file from which translation strings should be
   * loaded
   * @param target the map into which the strings should be loaded
   */
  public static void loadXMLAsStrings(String file,
                                          HashMap<String,String> target) {
    File xmlFile = ResourceManager.getFileResource("txt/xml/lang/"  +
                                                   Config.getLanguage() + "/" +
                                                   file + ".xml");
    try {
      DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
        .newInstance();
      DocumentBuilder documentBuilder =
                                    documentBuilderFactory.newDocumentBuilder();
      Document document = documentBuilder.parse(xmlFile);
      NodeList text = document.getElementsByTagName("translation");
      int len = text.getLength();
      String name;
      Node node;
      for (int i = 0; i < len; i++) {
        node = text.item(i);
        name = node.getAttributes().getNamedItem("name").getNodeValue();
        target.put(name, node.getTextContent());
      }
    } catch (ParserConfigurationException | SAXException | IOException ex) {
      LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
    }
  }
  
  /**
   * Saves the XML output produced by the passed function to a file.
   * 
   * @param file the file to which the output should be saved
   * @param nodes the function that will produce the file's contents
   */
  public static void saveToFile(File file, Function<Document,Node> nodes) {
    try {
      DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
        .newInstance();
      DocumentBuilder documentBuilder =
                                    documentBuilderFactory.newDocumentBuilder();
      Document document = documentBuilder.newDocument();
      document.appendChild(nodes.apply(document));
      TransformerFactory transformerFactory = TransformerFactory.newInstance();
      Transformer transformer = transformerFactory.newTransformer();
      transformer.setOutputProperty(OutputKeys.INDENT, "yes");
      transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount",
                                    "2");
      DOMSource source = new DOMSource(document);
      StreamResult outFile = new StreamResult(file);
      transformer.transform(source, outFile);
    } catch (TransformerException | ParserConfigurationException ex) {
      LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
    }
  }
  
  /**
   * Saves all of the My Units in the passed list for the current game mode.
   * 
   * @param myUnits the My Units to save
   */
  public static void saveMyUnits(ObservableList<MyUnit> myUnits) {
    File xmlFile = ResourceManager.getFileResource("txt/xml/userdata/" +
                                                   "MyUnits.xml");
    try {
      DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
        .newInstance();
      DocumentBuilder documentBuilder =
                                    documentBuilderFactory.newDocumentBuilder();
      Document document = documentBuilder.newDocument();
      Element root = document.createElement("myunits");
      myUnits.forEach((myUnit) -> root.appendChild(myUnit.toXML(document)));
      document.appendChild(root);
      TransformerFactory transformerFactory = TransformerFactory.newInstance();
      Transformer transformer = transformerFactory.newTransformer();
      transformer.setOutputProperty(OutputKeys.INDENT, "yes");
      transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount",
                                    "2");
      DOMSource source = new DOMSource(document);
      StreamResult file = new StreamResult(xmlFile);
      transformer.transform(source, file);
    } catch (TransformerException | ParserConfigurationException ex) {
      LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
    }
  }
}
