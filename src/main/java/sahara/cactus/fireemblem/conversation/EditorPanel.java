/*
 * Copyright (C) 2017 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import sahara.cactus.fireemblem.conversation.dialog.FileCreationDialog;
import sahara.cactus.fireemblem.conversation.dialog.MyUnitCustomizationDialog;
import sahara.cactus.fireemblem.conversation.dialog.SupportConversationCreationDialog;
import sahara.cactus.fireemblem.conversation.manager.CharacterManager;
import sahara.cactus.fireemblem.conversation.manager.Config;
import sahara.cactus.fireemblem.conversation.manager.ResourceManager;
import sahara.cactus.fireemblem.conversation.manager.XMLManager;
import sahara.cactus.fireemblem.conversation.myunit.MyUnit;
import sahara.cactus.fireemblem.conversation.ui.control.ConversationPane;
import sahara.cactus.fireemblem.conversation.ui.control.MessageArea;
import sahara.cactus.fireemblem.conversation.ui.control.Palette;
import sahara.cactus.fireemblem.conversation.util.io.FileUtils;
import sahara.cactus.fireemblem.conversation.util.text.ScriptHelper;

/**
 * Contains the editor's main controls.
 * 
 * @author Secretive Cactus
 */
public class EditorPanel extends VBox {
  
  /**
   * Provides logging for the class.
   */
  private static final Logger LOGGER =
                                  Logger.getLogger(EditorPanel.class.getName());
  
  /**
   * Serves as the editor's text simulator.
   */
  private ConversationPane conversationPane;
  
  /**
   * Contains the editor's active messages.
   */
  private TabPane messageTabs;
  
  /**
   * Contains the editor's menus.
   */
  private MenuBar menuBar;
  
  /**
   * Contains the standard file operations like creation, opening, and saving.
   */
  private Menu fileMenu;
  
  /**
   * A menu option for creating a new generic message archive.
   */
  private MenuItem fileNewArchive;
  
  /**
   * A menu option for creating a new support conversation.
   */
  private MenuItem fileNewConversation;
  
  /**
   * A menu option for opening a message archive.
   */
  private MenuItem fileOpenItem;
  
  /**
   * A menu option for saving the current file.
   */
  private MenuItem fileSaveItem;
  
  /**
   * A menu option for saving the current file with a different name.
   */
  private MenuItem fileSaveAsItem;
  
  /**
   * A menu option for closing the editor.
   */
  private MenuItem fileExitItem;
  
  /**
   * A menu for configuring My Unit settings.
   */
  private Menu myUnitMenu;
  
  /**
   * A toggle group for My Unit selection.
   */
  private ToggleGroup myUnitToggleGroup;
  
  /**
   * A menu for selecting the active My Unit.
   */
  private Menu myUnitSelectionMenu;
  
  /**
   * A menu option for opening the My Unit editor.
   */
  private MenuItem myUnitEditor;
  
  /**
   * A toggle-type menu item for using the active My Unit name in script mode.
   */
  private CheckMenuItem useMyUnitName;
  
  /**
   * A menu for configuring editor settings.
   */
  private Menu configMenu;
  
  /**
   * A toggle-type menu item for automatic conversion of messages to scripts.
   */
  private CheckMenuItem autoConvert;
  
  /**
   * A toggle-type menu item for determining whether or not backgrounds should
   * be displayed in the conversation simulator.
   */
  private CheckMenuItem showBackground;
  
  /**
   * A toggle-type menu item for determining whether or not sound should be
   * played by the editor.
   */
  private CheckMenuItem playSound;
  
  /**
   * A toggle group for selecting the active game mode.
   */
  private ToggleGroup gameModeGroup;
  
  /**
   * A menu for selecting the active game mode.
   */
  private Menu gameModeMenu;
  
  /**
   * A toggle-type menu item for activating Awakening mode.
   */
  private RadioMenuItem gameModeAwakening;
  
  /**
   * A toggle-type menu item for activating Fates mode.
   */
  private RadioMenuItem gameModeFates;
  
  /**
   * A toggle group for selecting the active language.
   */
  private ToggleGroup languageGroup;
  
  /**
   * A menu for selecting the active language.
   */
  private Menu languageMenu;
  
  /**
   * Creates a usable instance of the EditorPanel.
   */
  public EditorPanel() {
    super();
    addControls();
  }
  
  /**
   * Adds the panel's controls.
   */
  private void addControls() {
    createMenuBar();
    getChildren().add(menuBar);
    SplitPane splitPane = new SplitPane();
    splitPane.setOrientation(Orientation.VERTICAL);
    AnchorPane mainWindow = new AnchorPane();
    conversationPane = new ConversationPane();
    VBox v1 = new VBox();
    HBox buttons = new HBox(4);
    buttons.setAlignment(Pos.CENTER);
    VBox.setMargin(buttons, new Insets(4, 4, 4, 4));
    Button submit = new Button("");
    submit.textProperty().bind(XMLManager.getTextProperty("Reload_Button"));
    Button previous = new Button();
    previous.textProperty().bind(XMLManager.getTextProperty("Previous_Button"));
    previous.setDisable(true);
    Button next = new Button();
    next.textProperty().bind(XMLManager.getTextProperty("Next_Button"));
    previous.setOnAction((e) -> {
      try {
        conversationPane.displayPrevious(next, previous);
      } catch (Config.GameMode.GameBranchMissingException ex) {
        LOGGER.log(Level.SEVERE, "No branch exists for game mode " + 
                                 Config.getGameMode() + ".", ex);
      }
    });
    next.setOnAction((e) -> {
      try {
        conversationPane.displayNext(next, previous);
      } catch (Config.GameMode.GameBranchMissingException ex) {
        LOGGER.log(Level.SEVERE, "No branch exists for game mode " + 
                                 Config.getGameMode() + ".", ex);
      }
    });
    next.setDisable(true);
    buttons.getChildren().addAll(submit, previous, next);
    HBox buttons2 = new HBox(4);
    buttons2.setAlignment(Pos.CENTER);
    Button toGame = new Button();
    toGame.textProperty().bind(XMLManager.getTextProperty(
                                                     "Convert_To_Game_Button"));
    Button toScript = new Button();
    toScript.textProperty().bind(XMLManager.getTextProperty(
                                                   "Convert_To_Script_Button"));
    buttons2.getChildren().addAll(toGame, toScript);
    v1.getChildren().addAll(conversationPane, buttons, buttons2);
    VBox v2 = new VBox();
    messageTabs = new TabPane();
    messageTabs.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
    VBox.setVgrow(messageTabs, Priority.ALWAYS);
    CharacterManager.MESSAGE_TABS = messageTabs;
    v2.getChildren().add(messageTabs);
    toGame.setOnAction((e) -> {
      MessageArea area;
      if ((area = getCurrentEditor()) != null) {
        area.replaceText(ScriptHelper.convertFromScript(area.getText()));
        area.moveTo(0);
      }
    });
    toScript.setOnAction((e) -> {
      MessageArea area;
      if ((area = getCurrentEditor()) != null) {
        area.replaceText(ScriptHelper.convertToScript(area.getText()));
        area.moveTo(0);
      }
    });
    submit.setOnAction((e) -> {
      MessageArea area;
      if ((area = getCurrentEditor()) != null) {
        try {
          conversationPane.setText(area.getText(), next, previous);
        } catch (Config.GameMode.GameBranchMissingException ex) {
          LOGGER.log(Level.SEVERE, "No branch exists for game mode " + 
                                   Config.getGameMode() + ".", ex);
        }
      }
    });
    mainWindow.getChildren().addAll(v1, v2);
    AnchorPane.setLeftAnchor(v1, 4.0);
    AnchorPane.setTopAnchor(v1, 4.0);
    AnchorPane.setBottomAnchor(v1, 4.0);
    AnchorPane.setLeftAnchor(v2, 408.0);
    AnchorPane.setTopAnchor(v2, 4.0);
    AnchorPane.setRightAnchor(v2, 4.0);
    AnchorPane.setBottomAnchor(v2, 4.0);
    splitPane.getItems().addAll(mainWindow, new Palette(messageTabs));
    VBox.setVgrow(splitPane, Priority.ALWAYS);
    getChildren().add(splitPane);
  }
  
  /**
   * Creates the panel's config menu.
   */
  private void createConfigMenu() {
    configMenu = new Menu();
    configMenu.textProperty().bind(XMLManager.getTextProperty("Options_Menu"));
    languageMenu = new Menu();
    languageMenu.textProperty().bind(XMLManager.getTextProperty(
                                                      "Options_Menu_Language"));
    languageGroup = new ToggleGroup();
    File[] lang = ResourceManager.getFileResource("txt/xml/lang")
                                 .listFiles((f) -> f.isDirectory());
    RadioMenuItem langItem;
    for (File f: lang) {
      langItem = new RadioMenuItem(f.getName());
      if (f.getName().equals(Config.getLanguage())) {
        langItem.setSelected(true);
      }
      langItem.setOnAction((e) -> {
        Config.setLanguage(((RadioMenuItem) e.getSource()).getText());
      });
      languageGroup.getToggles().add(langItem);
      languageMenu.getItems().add(langItem);
    }
    gameModeMenu = new Menu();
    gameModeMenu.textProperty().bind(XMLManager.getTextProperty(
                                                     "Options_Menu_Game_Mode"));
    gameModeGroup = new ToggleGroup();
    if (new File(Config.ROOT_DIRECTORY + "resources/awakening").exists()) {
      gameModeAwakening = new RadioMenuItem();
      gameModeAwakening.setUserData(Config.GameMode.Awakening);
      gameModeAwakening.textProperty().bind(XMLManager.getTextProperty(
                                                             "Game_Awakening"));
      if (Config.getGameMode().isAwakening()) {
        gameModeAwakening.setSelected(true);
      }
      gameModeAwakening.setOnAction((ActionEvent e) -> {
        Config.setGameMode(Config.GameMode.Awakening);
      });
      gameModeAwakening.setToggleGroup(gameModeGroup);
      gameModeMenu.getItems().add(gameModeAwakening);
    }
    if (new File(Config.ROOT_DIRECTORY + "resources/fates").exists()) {
      gameModeFates = new RadioMenuItem();
      gameModeFates.setUserData(Config.GameMode.Fates);
      gameModeFates.textProperty().bind(XMLManager.getTextProperty(
                                                                 "Game_Fates"));
      if (Config.getGameMode().isFates()) {
        gameModeFates.setSelected(true);
      }
      gameModeFates.setOnAction((ActionEvent e) -> {
        Config.setGameMode(Config.GameMode.Fates);
      });
      gameModeFates.setToggleGroup(gameModeGroup);
      gameModeMenu.getItems().add(gameModeFates);
    }
    Config.gameModeProperty().addListener((o, ov, nv) -> {
      ObservableList<MenuItem> items = gameModeMenu.getItems();
      int len = items.size();
      for (int i = 0; i < len; i++) {
        if (nv.equals(items.get(i).getUserData())) {
          ((RadioMenuItem) items.get(i)).setSelected(true);
          break;
        }
      }
    });
    autoConvert = new CheckMenuItem();
    autoConvert.textProperty().bind(XMLManager.getTextProperty(
                                           "Options_Menu_Auto_Text_To_Script"));
    autoConvert.selectedProperty().bindBidirectional(
                                          Config.autoconvertToScriptProperty());
    playSound = new CheckMenuItem();
    playSound.textProperty().bind(XMLManager.getTextProperty(
                                                    "Options_Menu_Play_Sound"));
    playSound.selectedProperty().bindBidirectional(Config.playSoundProperty());
    showBackground = new CheckMenuItem();
    showBackground.textProperty().bind(XMLManager.getTextProperty(
                                               "Options_Menu_Show_Background"));
    showBackground.selectedProperty().bindBidirectional(
                                               Config.showBackgroundProperty());
    configMenu.getItems().addAll(languageMenu, gameModeMenu, playSound,
                                 autoConvert, showBackground);
    menuBar.getMenus().add(configMenu);
  }
  
  /**
   * Creates the panel's file menu.
   */
  private void createFileMenu() {
    fileMenu = new Menu();
    fileMenu.textProperty().bind(XMLManager.getTextProperty("File_Menu"));
    fileNewArchive = new MenuItem();
    fileNewArchive.textProperty().bind(XMLManager.getTextProperty(
                                                      "File_Menu_New_Archive"));
    fileNewArchive.setOnAction((ActionEvent e) -> {
      FileCreationDialog.createAndShow(messageTabs);
    });
    fileNewConversation = new MenuItem();
    fileNewConversation.textProperty().bind(XMLManager.getTextProperty(
                                                      "File_Menu_New_Support"));
    fileNewConversation.setOnAction((ActionEvent e) -> {
      SupportConversationCreationDialog.createAndShow(messageTabs);
    });
    fileOpenItem = new MenuItem();
    fileOpenItem.textProperty().bind(XMLManager.getTextProperty(
                                                             "File_Menu_Open"));
    fileOpenItem.setOnAction((ActionEvent e) -> {
      try {
        getScene().setCursor(Cursor.WAIT);
        FileUtils.openFile(getScene().getWindow(), messageTabs);
      } finally {
        getScene().setCursor(Cursor.DEFAULT);
      }
    });
    fileSaveItem = new MenuItem();
    fileSaveItem.textProperty().bind(XMLManager.getTextProperty(
                                                             "File_Menu_Save"));
    fileSaveItem.setOnAction((ActionEvent e) -> {
      try {
        getScene().setCursor(Cursor.WAIT);
        FileUtils.saveFile(getScene().getWindow());
      } finally {
        getScene().setCursor(Cursor.DEFAULT);
      }
    });
    fileSaveAsItem = new MenuItem();
    fileSaveAsItem.textProperty().bind(XMLManager.getTextProperty(
                                                          "File_Menu_Save_As"));
    fileSaveAsItem.setOnAction((ActionEvent e) -> {
      try {
        getScene().setCursor(Cursor.WAIT);
        FileUtils.saveFileAs(getScene().getWindow());
      } finally {
        getScene().setCursor(Cursor.DEFAULT);
      }
    });
    fileExitItem = new MenuItem();
    fileExitItem.textProperty().bind(XMLManager.getTextProperty(
                                                             "File_Menu_Exit"));
    fileExitItem.setOnAction((ActionEvent e) -> {
      Platform.exit();
    });
    fileMenu.getItems().addAll(
      fileNewArchive, fileNewConversation, fileOpenItem, fileSaveItem,
      fileSaveAsItem, fileExitItem
    );
    menuBar.getMenus().add(fileMenu);
  }
  
  /**
   * Creates the panel's menu bar.
   */
  private void createMenuBar() {
    menuBar = new MenuBar();
    createFileMenu();
    createMyUnitMenu();
    createConfigMenu();
  }
  
  /**
   * Creates the panel's My Unit menu.
   */
  private void createMyUnitMenu() {
    myUnitMenu = new Menu();
    myUnitMenu.textProperty().bind(XMLManager.getTextProperty("My_Unit_Menu"));
    createMyUnitSelectionMenu();
    myUnitEditor = new MenuItem();
    myUnitEditor.textProperty().bind(XMLManager.getTextProperty(
                                                "My_Unit_Menu_My_Unit_Editor"));
    myUnitEditor.setOnAction((e) -> {
      try {
        MyUnitCustomizationDialog.createAndShow();
      } catch (Config.GameMode.GameBranchMissingException ex) {
        LOGGER.log(Level.SEVERE, "No branch exists for game mode " + 
                                 Config.getGameMode() + ".", ex);
      }
    });
    useMyUnitName = new CheckMenuItem();
    useMyUnitName.textProperty().bind(XMLManager.getTextProperty(
                                   "My_Unit_Menu_Use_My_Unit_Name_In_Scripts"));
    useMyUnitName.selectedProperty().bindBidirectional(
                                       Config.useMyUnitNameInScriptsProperty());
    myUnitMenu.getItems().addAll(myUnitEditor, useMyUnitName);
    menuBar.getMenus().add(myUnitMenu);
  }
  
  /**
   * Creates the panel's My Unit selection menu.
   */
  private void createMyUnitSelectionMenu() {
    myUnitSelectionMenu = new Menu();
    myUnitSelectionMenu.textProperty().bind(XMLManager.getTextProperty(
                                                       "My_Unit_Menu_My_Unit"));
    myUnitToggleGroup = new ToggleGroup();
    RadioMenuItem rItem;
    for (MyUnit mu : CharacterManager.MY_UNITS) {
      rItem = new RadioMenuItem();
      rItem.setUserData(mu);
      rItem.textProperty().bind(mu.nameProperty());
      myUnitToggleGroup.getToggles().add(rItem);
      if (mu == CharacterManager.getMyUnit()) {
        rItem.setSelected(true);
      }
      rItem.setOnAction((e) -> {
        RadioMenuItem source = (RadioMenuItem) e.getSource();
        CharacterManager.setMyUnit((MyUnit) source.getUserData());
      });
      myUnitSelectionMenu.getItems().add(rItem);
    }
    CharacterManager.myUnitProperty().addListener((o, ov, nv) -> {
      int i;
      if ((i = CharacterManager.MY_UNITS.indexOf(nv)) > -1) {
        ((RadioMenuItem) myUnitSelectionMenu.getItems().get(i))
          .setSelected(true);
      }
    });
    CharacterManager.MY_UNITS.addListener(
                             (ListChangeListener.Change‌<? extends MyUnit> c) ->{
      while (c.next()) {
        if (c.wasAdded()) {
          c.getAddedSubList().forEach((mu) -> {
            RadioMenuItem muItem = new RadioMenuItem();
            muItem.setUserData(mu);
            muItem.textProperty().bind(mu.nameProperty());
            myUnitToggleGroup.getToggles().add(muItem);
            muItem.setOnAction((e) -> {
              RadioMenuItem source = (RadioMenuItem) e.getSource();
              CharacterManager.setMyUnit((MyUnit) source.getUserData());
            });
            myUnitSelectionMenu.getItems().add(muItem);
          });
        } else if (c.wasRemoved()) {
          int start = c.getFrom();
          int end = start + c.getRemovedSize();
          RadioMenuItem muItem;
          while (end > start) {
            muItem = (RadioMenuItem) myUnitSelectionMenu.getItems().get(start);
            muItem.textProperty().unbind();
            muItem.setOnAction(null);
            myUnitToggleGroup.getToggles().remove(muItem);
            myUnitSelectionMenu.getItems().remove(start);
            end--;
          }
        }
      }
    });
    myUnitMenu.getItems().add(myUnitSelectionMenu);
  }
  
  /**
   * Gets the active MessageArea.
   * 
   * @return the active MessageArea
   */
  private MessageArea getCurrentEditor() {
    Tab tab;
    if ((tab = messageTabs.getSelectionModel().getSelectedItem()) != null) {
      return (MessageArea) tab.getContent();
    } else {
      return null;
    }
  }
  
  /**
   * Gets the panel's tab pane.
   * 
   * @return the panel's tab pane
   */
  public TabPane getMessageTabs() {
    return messageTabs;
  }
}
