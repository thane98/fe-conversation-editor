/*
 * Copyright (C) 2017 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.myunit;

import java.util.HashMap;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import sahara.cactus.fireemblem.conversation.manager.XMLManager;
import sahara.cactus.fireemblem.conversation.wrapper.CharacterBust;

/**
 * Contains My Unit data that can be drawn onto a Canvas.
 * 
 * @author Secretive Cactus
 */
public abstract class MyUnit extends CharacterBust {
  
  /**
   * The My Unit's gender.
   */
  protected Gender gender;
  
  /**
   * The My Unit's body index.
   */
  protected int body;
  
  /**
   * The My Unit's eye index.
   */
  protected int eyes;
  
  /**
   * The My Unit's hair style index.
   */
  protected int hairStyle;
  
  /**
   * The My Unit's voice index.
   */
  protected String voice;
  
  /**
   * The map of hair colors for children that is associated with the My Unit.
   */
  protected HashMap<String,String> childHairColors;
  
  /**
   * Contains the available genders for My Units.
   */
  public enum Gender {
    /**
     * Used for female My Units.
     */
    female,
    
    /**
     * Used for male My Units.
     */
    male;
  }
  
  /**
   * Creates a new My Unit with the default name for the current game mode.
   */
  public MyUnit() {
    super(XMLManager.getText("Default_My_Unit_Name"));
    gender = Gender.female;
    eyes = 0;
    body = 0;
    hairStyle = 0;
    voice = "1";
    childHairColors = new HashMap<>();
  }
  
  /**
   * Creates a My Unit with the given name.
   * 
   * @param name the My Unit's name
   */
  public MyUnit(String name) {
    super(name);
    gender = Gender.female;
    eyes = 0;
    body = 0;
    hairStyle = 0;
    voice = "1";
    childHairColors = new HashMap<>();
  }
  
  /**
   * Gets the prefix of the My Unit's portrait images.
   * 
   * @return the prefix of the My Unit's portrait images
   */
  public abstract String getCharacterImageName();
  
  /**
   * Gets the map of hair colors for children that is associated with the My
   * Unit.
   * 
   * @return the map of hair colors for children that is associated with the My
   * Unit
   */
  public HashMap<String,String> getChildHairColors() {
    return childHairColors;
  }
  
  /**
   * Gets the My Unit's DatId to allow its display offsets to be found.
   * 
   * @param type "ST" for a stage image or "BU" for a bust image
   * @return the My Unit's DatId
   */
  public abstract String getDatId(String type);
  
  /**
   * Gets the My Unit's body index.
   * 
   * @return the My Unit's body index
   */
  public int getBody() {
    return body;
  }
  
  /**
   * Gets the My Unit's eye index.
   * 
   * @return the My Unit's eye index
   */
  public int getEyes() {
    return eyes;
  }
  
  /**
   * Gets the My Unit's gender.
   * 
   * @return the My Unit's gender
   */
  public Gender getGender() {
    return gender;
  }
  
  /**
   * Gets the My Unit's hair file name.
   * 
   * @param type "st" for a stage image or "bu" for a bust image
   * @return the My Unit's hair file name
   */
  public abstract String getHairName(String type);
  
  /**
   * Gets the My Unit's hair style index.
   * 
   * @return the My Unit's hair style index
   */
  public int getHairStyle() {
    return hairStyle;
  }
  
  /**
   * Gets the My Unit's voice index.
   * 
   * @return the My Unit's voice index
   */
  public String getVoice() {
    return voice;
  }
  
  /**
   * Gets whether or not the My Unit is female.
   * 
   * @return whether or not the My Unit is female
   */
  public boolean isFemale() {
    return gender == Gender.female;
  }
  
  /**
   * Sets the My Unit's body index.
   * 
   * @param value the My Unit's body index
   */
  public void setBody(int value) {
    body = value;
  }
  
  /**
   * Sets the My Unit's eye index.
   * 
   * @param value the My Unit's eye index
   */
  public void setEyes(int value) {
    eyes = value;
  }
  
  /**
   * Sets the My Unit's gender.
   * 
   * @param value the My Unit's gender
   */
  public void setGender(Gender value) {
    gender = value;
  }
  
  /**
   * Sets the My Unit's hair style index.
   * 
   * @param value the My Unit's hair style index
   */
  public void setHairStyle(int value) {
    hairStyle = value;
  }
  
  /**
   * Sets the My Unit's voice index.
   * 
   * @param value the My Unit's voice index
   */
  public void setVoice(String value) {
    voice = value;
  }
  
  /**
   * Converts the My Unit to an XML representation of itself for saving.
   * 
   * @param document the document in which the My Unit will be saved
   * @return the My Unit's base XML node
   */
  public abstract Node toXML(Document document);
  
}
