/*
 * Copyright (C) 2016 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.TransferMode;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import sahara.cactus.fireemblem.conversation.dialog.FileCreationDialog;
import sahara.cactus.fireemblem.conversation.dialog.SupportConversationCreationDialog;
import sahara.cactus.fireemblem.conversation.manager.Config;
import sahara.cactus.fireemblem.conversation.manager.Config.GameMode;
import sahara.cactus.fireemblem.conversation.manager.Config.GameMode.GameBranchMissingException;
import sahara.cactus.fireemblem.conversation.manager.ResourceManager;
import sahara.cactus.fireemblem.conversation.manager.XMLManager;
import sahara.cactus.fireemblem.conversation.util.io.FileUtils;
import sahara.cactus.fireemblem.conversation.util.io.ForkedOutputStream;

/**
 * The main application class for the editor.
 * 
 * @author Secretive Cactus
 */
public class FireEmblemConversationEditor extends Application {
  
  /**
   * Provides logging for the class.
   */
  private static final Logger LOGGER =
                 Logger.getLogger(FireEmblemConversationEditor.class.getName());
  
  /**
   * The editor's main stage.
   */
  private Stage stage;
  
  /**
   * The editor's root scene.
   */
  private Scene mainScene;
  
  /**
   * Contains the editor's displayed contents.
   */
  private EditorPanel editorPanel;
  
  /**
   * Adds accelerators to the application.
   */
  private void addAccelerators() {
    mainScene.getAccelerators().put(
                 new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN),
            () -> FileCreationDialog.createAndShow(editorPanel.getMessageTabs())
                                   );
    mainScene.getAccelerators().put(
                 new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN,
                                        KeyCombination.SHIFT_DOWN), () -> {
                                SupportConversationCreationDialog.createAndShow(
                                                  editorPanel.getMessageTabs());
                                        });
    mainScene.getAccelerators().put(
                 new KeyCodeCombination(KeyCode.O, KeyCombination.CONTROL_DOWN),
                                   () -> {
                                      try {
                                        mainScene.setCursor(Cursor.WAIT);
                        FileUtils.openFile(stage, editorPanel.getMessageTabs());
                                      } finally {
                                        mainScene.setCursor(Cursor.DEFAULT);
                                      }
                                    });
    mainScene.getAccelerators().put(
                 new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN),
                                   () -> {
                                      try {
                                        mainScene.setCursor(Cursor.WAIT);
                                        FileUtils.saveFile(stage);
                                      } finally {
                                        mainScene.setCursor(Cursor.DEFAULT);
                                      }
                                    });
    mainScene.getAccelerators().put(
                  new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN,
                                         KeyCombination.SHIFT_DOWN),
                                 () -> {
                                    try {
                                      mainScene.setCursor(Cursor.WAIT);
                                      FileUtils.saveFileAs(stage);
                                    } finally {
                                      mainScene.setCursor(Cursor.DEFAULT);
                                    }
                                  });
    mainScene.getAccelerators().put(
                 new KeyCodeCombination(KeyCode.Q, KeyCombination.CONTROL_DOWN),
                                    () -> Platform.exit()
                                   );
  }
  
  /**
   * Creates the application's contents and displays them.
   */
  @Override
  public void start(Stage primaryStage) {
    stage = primaryStage;
    editorPanel = new EditorPanel();
    mainScene = new Scene(editorPanel, 1000, 800);
    try {
      mainScene.getStylesheets().add(
                    ResourceManager.getFileResource("txt/css/lexer.css").toURI()
                                     .toURL().toExternalForm());
    } catch (MalformedURLException ex) {
      LOGGER.log(Level.SEVERE, ex.toString(), ex);
    }
    mainScene.setOnDragOver((DragEvent e) -> {
      Dragboard d = e.getDragboard();
      if (d.hasFiles() && d.getFiles().size() > 0) {
        String name = d.getFiles().get(0).getName().toLowerCase();
        if (name.endsWith(".txt") || name.endsWith(".bin") ||
            name.endsWith(".bin.lz") || name.endsWith(".fescript")) {
          e.acceptTransferModes(TransferMode.COPY);
        }
      } else {
        e.consume();
      }
    });
    mainScene.setOnDragDropped((DragEvent e) -> {
      Dragboard d = e.getDragboard();
      if (d.hasFiles() && d.getFiles().size() > 0) {
        File file = d.getFiles().get(0);
        Platform.runLater(() -> {
          try {
            mainScene.setCursor(Cursor.WAIT);
            e.setDropCompleted(FileUtils.openFile(file,
                                                 editorPanel.getMessageTabs()));
          } finally {
            mainScene.setCursor(Cursor.DEFAULT);
          }
        });
      } else {
        e.setDropCompleted(false);
      }
      e.consume();
    });
    addAccelerators();
    primaryStage.titleProperty().bind(XMLManager.getTextProperty("App_Name"));
    primaryStage.setOnCloseRequest((WindowEvent e) -> Platform.exit());
    primaryStage.setScene(mainScene);
    primaryStage.show();
  }
  
  /**
   * Launches the application.
   * 
   * @param args arguments passed to the application
   * @throws GameBranchMissingException when a branch has not been supplied for
   * a game mode
   * @throws java.io.FileNotFoundException if the log files cannot be found or
   * created
   */
  public static void main(String[] args) throws GameBranchMissingException,
                                                FileNotFoundException {
    System.setOut(new PrintStream(new ForkedOutputStream(
                                             System.out, new File("out.log"))));
    System.setErr(new PrintStream(new ForkedOutputStream(
                                             System.err, new File("err.log"))));
    Config.loadConfiguration();
    if (Config.getGameMode() == null) {
      if (new File(Config.ROOT_DIRECTORY + "resources/awakening").exists()) {
        Config.setGameMode(GameMode.Awakening);
      } else if (new File(Config.ROOT_DIRECTORY + "resources/fates").exists()) {
        Config.setGameMode(GameMode.Fates);
      } else {
        throw new IllegalArgumentException("No game modes are installed!");
      }
    }
    launch(args);
  } 
}
