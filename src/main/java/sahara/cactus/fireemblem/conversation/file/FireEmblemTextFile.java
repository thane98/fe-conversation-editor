/*
 * Copyright (C) 2017 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.file;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import sahara.cactus.fireemblem.conversation.wrapper.FireEmblemMessage;

/**
 * Represents an instance of a Fire Emblem text file. Contains all of the
 * messages in the file and has methods related to writing back output.
 * 
 * @author Secretive Cactus
 */
public class FireEmblemTextFile {
  
  /**
   * Provides logging for the class.
   */
  private static final Logger LOGGER =
                           Logger.getLogger(FireEmblemTextFile.class.getName());
  
  /**
   * The name of the file, used as an identifier and default file name.
   */
  protected String name;
  
  /**
   * Contains the file's messages.
   */
  protected final ArrayList<FireEmblemMessage> messages;
  
  /**
   * Creates a new text file with no name and an empty list of messages.
   */
  public FireEmblemTextFile() {
    messages = new ArrayList<>();
  }
  
  /**
   * Creates a new text file with the given name and an empty list of messages.
   * 
   * @param name the name of the text file
   */
  public FireEmblemTextFile(String name) {
    this.name = name;
    messages = new ArrayList<>();
  }
  
  /**
   * Closes the text file.
   */
  public void close() {
    messages.forEach((message) -> message.contentProperty().unbind());
  }
  
  /**
   * Checks whether or not any of the messages in the file have the specified
   * name.
   * 
   * @param name the name of the message to search for
   * @return whether or not the message is present
   */
  public boolean containsMessage(String name) {
    for (FireEmblemMessage message : messages) {
      if (message.getName().equals(name)) {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Returns the text file's array of content.
   * @return the text file's array of content
   */
  public ArrayList<FireEmblemMessage> getMessages() {
    return messages;
  }
  
  /**
   * Returns the text file's name.
   * @return the text file's name
   */
  public String getName() {
    return name;
  }
  
  /**
   * Returns the number of messages contained in the text file.
   * @return the number of messages contained in the text file
   */
  public int getNumMessages() {
    return messages.size();
  }
  
  /**
   * Removes the message at the given index from the file.
   * 
   * @param index the index of the message to remove from the file
   */
  public void removeMessage(int index) {
    FireEmblemMessage message = messages.get(index);
    if (message != null) {
      message.contentProperty().unbind();
      messages.remove(index);
    }
  }
  
  /**
   * Renames one of the file's messages.
   * 
   * @param index the index of the message
   * @param newName the new name of the message
   */
  public void renameMessage(int index, String newName) {
    FireEmblemMessage message = messages.get(index);
    if (message != null) {
      message.setName(newName);
    }
  }
  
  /**
   * Converts the text file's messages into a format suitable for saving as a
   * .bin file.
   * 
   * @return the lines that should be converted into a .bin file
   */
  public String[] toBinOutput() {
    ArrayList<String> output = new ArrayList<>();
    output.add("MESS_ARCHIVE_" + name);
    output.add("");
    output.add("");
    output.add("Message Name: Message");
    output.add("");
    output.add("");
    String keyName; 
    for (FireEmblemMessage message : messages) {
      try {
        keyName = new String(("MID_" + name + "_" + message.getName() + ": ")
                             .getBytes("Shift_JIS"), "Shift_JIS");
      } catch (UnsupportedEncodingException ex) {
        LOGGER.log(Level.SEVERE, ex.toString(), ex);
        return null;
      }
      output.add(keyName + message.getContentForOut());
    }
    return output.toArray(new String[output.size()]);
  }
  
  /**
   * Converts the text file's messages into a format suitable for saving as a
   * .fescript file.
   * 
   * @return the lines that should be saved as a .fescript file
   */
  public List<String> toScriptOutput() {
    ArrayList<String> output = new ArrayList<>();
    output.add("MESS_ARCHIVE_" + name);
    output.add("");
    output.add("");
    output.add("Message Name: Message");
    output.add("");
    output.add("");
    String keyName; 
    for (FireEmblemMessage message : messages) {
      try {
        keyName = new String(("MID_" + name + "_" + message.getName() + ": ")
                             .getBytes("Shift_JIS"), "Shift_JIS");
      } catch (UnsupportedEncodingException ex) {
        LOGGER.log(Level.SEVERE, ex.toString(), ex);
        return null;
      }
      output.add(keyName + message.getContentForOutScript());
    }
    return output;
  }
  
  /**
   * Converts the text file's messages into a format suitable for saving as a
   * .txt file.
   * 
   * @return the lines that should be saved as a .txt file
   */
  public List<String> toTextOutput() {
    ArrayList<String> output = new ArrayList<>();
    output.add("MESS_ARCHIVE_" + name);
    output.add("");
    output.add("");
    output.add("Message Name: Message");
    output.add("");
    output.add("");
    String keyName; 
    for (FireEmblemMessage message : messages) {
      try {
        keyName = new String(("MID_" + name + "_" + message.getName() + ": ")
                             .getBytes("Shift_JIS"), "Shift_JIS");
      } catch (UnsupportedEncodingException ex) {
        LOGGER.log(Level.SEVERE, ex.toString(), ex);
        return null;
      }
      output.add(keyName + message.getContentForOut());
    }
    return output;
  }
}
