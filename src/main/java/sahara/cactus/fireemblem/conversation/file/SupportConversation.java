/*
 * Copyright (C) 2016 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.file;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import sahara.cactus.fireemblem.conversation.manager.Config;
import sahara.cactus.fireemblem.conversation.manager.XMLManager;
import sahara.cactus.fireemblem.conversation.wrapper.FireEmblemMessage;

/**
 * Represents an instance of a Fire Emblem support conversation. Contains all of
 * the messages in the file and has methods related to writing back output.
 * 
 * @author Secretive Cactus
 */
public class SupportConversation extends FireEmblemTextFile {
  
  /**
   * Provides logging for the class.
   */
  private static final Logger LOGGER = Logger.getLogger(
                                           SupportConversation.class.getName());
  
  /**
   * The name of the first of the two characters in the support.
   */
  private final String characterOne;
  
  /**
   * The name of the second of the two characters in the support.
   */
  private final String characterTwo;
  
  /**
   * The rank to which the support goes as a number, from C as 1 to S as 4.
   * Should always be either three or four.
   */
  private final int rank;
  
  /**
   * Creates a support conversation of the given type between a pair of
   * characters.
   * 
   * @param characterOne the Japanese name for the first character in the
   * support
   * @param characterTwo the Japanese name for the second character in the
   * support
   * @param type the type of support, whether normal, parental, or sibling. The
   * actual available values are determined by the current language's XML system
   * file.
   */
  public SupportConversation(String characterOne, String characterTwo, 
                             String type) {
    this.characterOne = characterOne;
    this.characterTwo = characterTwo;
    if (type.equals(XMLManager.getText("Support_Conversation_Type_Parent"))) {
      name = characterOne + "_" + characterTwo + "_親子";
    } else if (type.equals(XMLManager.getText(
                                       "Support_Conversation_Type_Siblings"))) {
      name = characterOne + "_" + characterTwo + "_兄弟";
    } else {
      name = characterOne + "_" + characterTwo;
    }
    this.rank = 3;
  }
  
  /**
   * Creates a support conversation of the given type between a pair of
   * characters.
   * 
   * @param characterOne the Japanese name for the first character in the
   * support
   * @param characterTwo the Japanese name for the second character in the
   * support
   * @param rank the rank to which the support goes as a number, from C as 1 to
   * S as 4. Should always be either three or four.
   * @param type the type of support, whether normal, parental, or sibling. The
   * actual available values are determined by the current language's XML system
   * file.
   */
  public SupportConversation(String characterOne, String characterTwo, int rank,
                             String type) {
    this.characterOne = characterOne;
    this.characterTwo = characterTwo;
    if (type.equals(XMLManager.getText("Support_Conversation_Type_Parent"))) {
      name = characterOne + "_" + characterTwo + "_親子";
    } else if (type.equals(
                    XMLManager.getText("Support_Conversation_Type_Siblings"))) {
      name = characterOne + "_" + characterTwo + "_兄弟";
    } else {
      name = characterOne + "_" + characterTwo;
    }
    this.rank = rank;
  }
  
  /**
   * Resets the support conversation so that it has empty copies of each of its
   * required text keys.
   */
  public void initializeContents() {
    messages.clear();
    if (Config.getGameMode().isAwakening() &&
        (characterOne.startsWith("プレイヤー") ||
         characterTwo.startsWith("プレイヤー"))) {
      int count = rank * 3;
      for (int i = 0; i < count; i++) {
        messages.add(new FireEmblemMessage(shortId(i % rank, (i / rank) + 1)));
      }
    } else {
      for (int i = 0; i < rank; i++) {
        messages.add(new FireEmblemMessage(shortId(i)));
      }
    }
  }
  
  /**
   * Returns the first of the two characters in the support conversation.
   * 
   * @return the first of the two characters in the support conversation
   */
  public String getCharacterOne() {
    return characterOne;
  }
  
  /**
   * Returns the second of the two characters in the support conversation.
   * 
   * @return the second of the two characters in the support conversation
   */
  public String getCharacterTwo() {
    return characterTwo;
  }
  
  /**
   * Returns the content of the text key for the support of the requested rank.
   * 
   * @param rank the rank of the desired support as C, B, A, or S
   * @return the content of the text key for the support rank
   * @throws IndexOutOfBoundsException if the requested rank is higher than the
   * support conversation's max rank
   */
  public String getConversation(char rank) {
    switch (rank) {
      case 'C': return messages.get(0).getContent();
      case 'B': return messages.get(1).getContent();
      case 'A': return messages.get(2).getContent();
      case 'S': return messages.get(3).getContent();
      default: return null;
    }
  }
  
  /**
   * Returns the maximum rank of the support conversation as an integer, up to
   * 4 as S.
   * 
   * @return the maximum rank of the support conversation as an integer 
   */
  public int getRank() {
    return rank;
  }
  
  /**
   * Gets the text key for a support with the given rank.
   * 
   * @param rank the rank of the support as an integer, starting with 0 for C
   * @return the text key for a support with the given rank
   */
  public String shortId(int rank) {
    StringBuilder supportId = new StringBuilder(40);
    supportId.append(name);
    switch (rank) {
      case 0:
        supportId.append("_Ｃ");
        break;
      case 1:
        supportId.append("_Ｂ");
        break;
      case 2:
        supportId.append("_Ａ");
        break;
      case 3:
        supportId.append("_Ｓ");
        break;
    }
    if (characterOne.equals("プレイヤー女") ||
        characterTwo.equals("プレイヤー女")) {
      supportId.append("_PCF1");
    } else if (characterOne.equals("プレイヤー男") ||
               characterTwo.equals("プレイヤー男")) {
      supportId.append("_PCM1");
    }
    return supportId.toString();
  }
  
  /**
   * Gets the text key for a support with the given rank and player height ID,
   * for Awakening's unusual system.
   * 
   * @param rank the rank of the support as an integer, starting with 0 for C
   * @param playerNum the player height ID
   * @return the text key for a support with the given rank
   */
  public String shortId(int rank, int playerNum) {
    StringBuilder supportId = new StringBuilder(40);
    supportId.append(name);
    switch (rank) {
      case 0:
        supportId.append("_Ｃ");
        break;
      case 1:
        supportId.append("_Ｂ");
        break;
      case 2:
        supportId.append("_Ａ");
        break;
      case 3:
        supportId.append("_Ｓ");
        break;
    }
    if (characterOne.equals("プレイヤー女") ||
        characterTwo.equals("プレイヤー女")) {
      supportId.append("_PCF").append(playerNum);
    } else if (characterOne.equals("プレイヤー男") ||
               characterTwo.equals("プレイヤー男")) {
      supportId.append("_PCM").append(playerNum);
    }
    return supportId.toString();
  }
  
  /**
   * Returns the passed key formatted for saving as a text key.
   * 
   * @param key the key to format
   * @return the formatted key
   */
  public String textKey(String key) {
    try {
      return new String(("MID_支援_" + key + ": ").getBytes("Shift_JIS"),
                        "Shift_JIS");
    } catch (UnsupportedEncodingException ex) {
      LOGGER.log(Level.SEVERE, ex.toString(), ex);
      return null;
    }
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public String[] toBinOutput() {
    ArrayList<String> output = new ArrayList<>();
    output.add("MESS_ARCHIVE_" + name);
    output.add("");
    output.add("");
    output.add("Message Name: Message");
    output.add("");
    output.add("");
    messages.forEach((message) -> {
      output.add(textKey(message.getName()) + message.getContentForOut());
    });
    return output.toArray(new String[output.size()]);
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public List<String> toScriptOutput() {
    ArrayList<String> output = new ArrayList<>();
    output.add("MESS_ARCHIVE_" + name);
    output.add("");
    output.add("");
    output.add("Message Name: Message");
    output.add("");
    output.add("");
    messages.forEach((message) -> {
      output.add(textKey(message.getName()) + message.getContentForOutScript());
    });
    return output;
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public List<String> toTextOutput() {
    ArrayList<String> output = new ArrayList<>();
    output.add("MESS_ARCHIVE_" + name);
    output.add("");
    output.add("");
    output.add("Message Name: Message");
    output.add("");
    output.add("");
    messages.forEach((message) -> {
      output.add(textKey(message.getName()) + message.getContentForOut());
    });
    return output;
  }
}
