/*
 * Copyright (C) 2017 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.util.text;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import sahara.cactus.fireemblem.conversation.manager.ResourceManager;
import sahara.cactus.fireemblem.conversation.manager.XMLManager;

/**
 * A utility class containing methods related to processing control codes in
 * messages.
 * 
 * @author Secretive Cactus
 */
public final class ConversationFormatter {
  
  /**
   * Matches the $G text code, which is used to show different text depending on
   * whether the player character is female or male.
   */
  private static final Pattern G_PATTERN =
                                      Pattern.compile("\\$G([^,]+),([^|]+)\\|");
  
  /**
   * Formats the passed message, substituting control codes like $Nu and $G.
   * Will also perform line wrapping and pagination if the message is a standard
   * script.
   * 
   * @param msg the message to format
   * @return the formatted message
   */
  public static String formatLines(String msg) {
    boolean formatted = false;
    if (msg.startsWith("[" + XMLManager.getText("Script_Command_Script"))) {
      msg = ScriptHelper.convertFromScript(msg);
    } else if (msg.startsWith("[" +
                       XMLManager.getText("Script_Command_Formatted_Script"))) {
      msg = ScriptHelper.convertFromScript(msg);
      formatted = true;
    }
    if (!formatted) {
      boolean hasPerms = msg.contains("$a");
      final String eol = System.getProperty("line.separator");
      String[] lines = msg.split("(" + eol + "|\\$k\\$p|\\$k\\\\n)");
      String result = "";
      Matcher match;
      String maxLengthName = XMLManager.getText("Max_Length_Name");
      for (int i = 0; i < lines.length; i++) {
        int ind = lines[i].lastIndexOf("|") + 1;
        if (ind < 0) {
          ind = 0;
        }
        String modifiers = lines[i].substring(0, ind);
        if (hasPerms) {
          lines[i] = lines[i].replaceAll("\\$Nu", maxLengthName);
        }
        if ((match = G_PATTERN.matcher(lines[i])).find()) {
          ind = lines[i].substring(0, lines[i].indexOf("$G"))
                .lastIndexOf("|") + 1;
          modifiers = lines[i].substring(0, ind);
          ArrayList<Integer> gInds = new ArrayList<>();
          ArrayList<String> g = new ArrayList<>();
          ArrayList<String> matches = new ArrayList<>();
          do {
            gInds.add(match.start(0) - ind);
            matches.add(match.group(0));
            if (ResourceManager.getTextLength(match.group(1)) >
                ResourceManager.getTextLength(match.group(2))) {
              g.add(match.group(1));
              lines[i] = lines[i].replace(match.group(0), match.group(1));
            } else {
              g.add(match.group(2));
              lines[i] = lines[i].replace(match.group(0), match.group(2));
            }
          } while (match.find());
          String res = formatLine(lines[i].substring(ind));
          for (int gI = 0; gI < gInds.size(); gI++) {
            res = res.replace(res.substring(gInds.get(gI)),
                              res.substring(gInds.get(gI))
                              .replace(g.get(gI), matches.get(gI)));
          }
          result += modifiers + res.replaceAll(maxLengthName, "\\$Nu");
        } else {
          result += modifiers + formatLine(lines[i].substring(ind))
                                .replaceAll(maxLengthName, "\\$Nu");
        }
        if (hasPerms) {
          lines[i] = lines[i].replaceAll(maxLengthName, "\\$Nu");
        }
        if (i < lines.length - 1) {
          result += "$k$p";
        }
      }
      return result;
    }
    return msg;
  }
  
  /**
   * Formats the passed line, performing line wrapping and pagination.
   * Can also be used directly to format a small piece of text for display, but
   * will not handle most control characters.
   * 
   * @param msg the line to format
   * @return the formatted line
   */
  public static String formatLine(String msg) {
    if (msg.startsWith("[" + XMLManager.getText("Script_Command_Script"))) {
      msg = ScriptHelper.convertFromScript(msg);
    }
    if (msg.contains("$a")) {
      msg = msg.replaceAll("\\$a", "");
    }
    msg = msg.replaceAll("\\\\n", " ");
    String[] strings = msg.split(" ");
    String valid = "";
    String test = "";
    boolean newPage = false;
    for (String s : strings) {
      if (!test.isEmpty()) {
        test += " ";
      }
      test += s;
      if (ResourceManager.getTextLength(test) < 305) {
        if (!valid.isEmpty()) {
          valid += " ";
        }
        valid += s;
      } else {
        if (newPage) {
          valid += "$k$p" + s;
          newPage = false;
        } else {
          valid += "\\n" + s;
          newPage = true;
        }
        test = s;
      }
    }
    return valid;
  }
  
  /**
   * Gets a name whose width is exactly equivalent to the maximum player name
   * width in the current game mode.
   * 
   * @return a name whose width is exactly equivalent to the maximum player name
   * width in the current game mode
   */
  public static int maxNameLength() {
    return ResourceManager.getTextLength(XMLManager.getText("Max_Length_Name"));
  }
}
